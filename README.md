# Build docker image
sudo docker build -t docker-registry.cirrus:5000/php .

# Push docker image
sudo docker push docker-registry.cirrus:5000/php

# This is a base image used for building app images
